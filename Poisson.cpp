
#include "Poisson.h"


Poisson::Poisson(int lambda,int analog_in, int precision) {
      _lambda = lambda;
      _precision = precision;
      randomSeed(analogRead(analog_in));
   } 

int Poisson::nextRandom() {

   double gauss = random(_precision);
   gauss = gauss/_precision;
   return -_lambda*log(gauss);

}
