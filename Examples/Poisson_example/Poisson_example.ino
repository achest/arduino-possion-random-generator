/**
 Copyright by Alexander Chestnov 
 http://www.chestnov.com

*/

long randNumber;

#include <Poisson.h>

Poisson randomGenerator(150,0,1000);


void setup(){
   Serial.begin(115200);
}

void loop() {
  // print a random number with variance 150 
  randNumber =   randomGenerator.nextRandom();
 Serial.println(randNumber);  

  delay(50);
}
